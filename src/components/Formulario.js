import React, {Component} from 'react';

class FormularioGasto extends Component {

	// refs campos del formulario
	nombreGasto = React.createRef();
	cantidadGasto = React.createRef()

	crearGasto = (e) => {
		
		// Prevent the default
		e.preventDefault();
		// create object with data
		const gasto = {
			nombreGasto : this.nombreGasto.current.value,
			cantidadGasto : this.cantidadGasto.current.value
		}

		// console.log(gasto);
		
		// add and send for props
		this.props.agregarGasto(gasto);


		// reset the form
		e.currentTarget.reset();
	}

	render(){
		return(
			<form onSubmit={this.crearGasto}>
				<h2>Add your expense here</h2>

				<div className="campo">
						<label>Name of Expense</label>
						<input ref={this.nombreGasto} className="u-full-width" type="text" placeholder="Ex. Transport" />
				</div>

				<div className="campo">
						<label>Mount</label>
						<input ref={this.cantidadGasto} className="u-full-width" type="text" placeholder="Ex. 300" />
				</div>

				<input className="button-primary u-full-width" type="submit" value="Add" />
		</form>
		);
	}
}

export default FormularioGasto;