import React, { Component } from 'react';
import '../css/App.css';
import Header from './Header';
import Formulario from './Formulario';
import Listado from './Listado'
import {validarPresupuesto} from '../helper';

class App extends Component {

	state = {
		presupuesto : '',
		restante : '',
		gastos: {}
	}

	componentDidMount() {
		this.obtenerPresupuesto();
	}

	obtenerPresupuesto = () => {
		let presupuesto = prompt('What is your budget?' );

		let resultado = validarPresupuesto(presupuesto);
		if(resultado) {
			this.setState({
				presupuesto: presupuesto,
				restante: presupuesto
			})
		} else {
			this.obtenerPresupuesto();
		}
	}

	// Add a new expense to the State
	agregarGasto = gasto => {
		//take a copy of the this.state.
		const gastos = {...this.state.gastos};

		//Add the expense to the object from the state
		gastos[`gasto${Date.now()}`] = gasto;

		//put into State
		this.setState({
			gastos
		})
	}

  render() {
    return (
			<div className="App container">
				<Header
					titulo='Weekly Expense'
				/>
				<div className="contenido-principal contenido">
					<div className="row">
						<div className="one-half column">
							<Formulario 
								agregarGasto = {this.agregarGasto}
							/>
						</div>
						<div className="one-half column">
							<Listado
								gastos={this.state.gastos}
							/>
						</div>
					</div>
				</div>
			</div>
    );
  }
}

export default App;
